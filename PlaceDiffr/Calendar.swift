//
//  Calendar.swift
//  PlaceDiffr
//
//  Created by Cognitive Puppy LLC on 12/7/14.
//  Copyright (c) 2014 Cognitive Puppy LLC. All rights reserved.
//

import Foundation
import EventKit

class Calendar {
    let eventStore : EKEventStore?
    
    init() {
        // Get access to the iOS Calendar database
        eventStore = EKEventStore()
        
        // Ask for access to the iOS Calendar database
        eventStore?.requestAccessToEntityType(EKEntityTypeEvent, completion: { (granted: Bool, error : NSError! ) -> Void in
            
            println("Calendars access granted = \(granted)")
            
            // Deterine the default calendar
            let defaultCalendar = self.eventStore?.defaultCalendarForNewEvents;
            
            println("Default calendar name: \(defaultCalendar?.title)")
            
            // Get some events from the default calendar
            let dateNow = NSDate(timeIntervalSinceReferenceDate:0)
            let dateIn24hrs = NSDate(timeIntervalSinceReferenceDate:60 * 60 * 24)
            let calendars : [EKCalendar] = [defaultCalendar!]
            let predicate : NSPredicate! = self.eventStore?.predicateForEventsWithStartDate(dateNow, endDate: dateIn24hrs, calendars: calendars)
            
            let events = self.eventStore?.eventsMatchingPredicate(predicate)
            
            if let eventCount = events?.count {
                
                println("Got \(eventCount) events.")
                
                for index in 0..<eventCount {
                    var event = events![index] as EKEvent
                    let eventTitle = event.title!
                    println("Event \(index) has title \(eventTitle)")
                }
            }
            else {
                println("Event Count is nil")
            }
            
        })
        
        
        
        
        
    }
}